﻿using System;

namespace prototype_example
{
	class AnimalDeepCopy : ICloneable
	{
		public string Nombre { get; set; }
		public int Patas { get; set; }
		public Detalles Rasgos { get; set; }

		public AnimalDeepCopy(string nombre, int patas, Detalles rasgos)
		{
			Nombre = nombre;
			Patas = patas;
			Rasgos = rasgos;
		}

		public object Clone()
		{
			AnimalDeepCopy clone = MemberwiseClone() as AnimalDeepCopy;
			//clone.Rasgos = new Detalles(Rasgos.Color, Rasgos.Edad);
			clone.Rasgos = Rasgos.Clone() as Detalles;
			return clone;
		}

		public override string ToString()
		{
			return string.Format("Nombre: {0}, Nro de Patas: {1}, {2}", Nombre, Patas, Rasgos.ToString());
		}
	}

	class Detalles : ICloneable
	{
		public string Color { get; set; }
		public int Edad { get; set; } 

		public Detalles(string color, int edad)
		{
			Color = color;
			Edad = edad;
		}

		public override string ToString()
		{
			return string.Format("Color: {0}, Edad: {1}", Color, Edad);
		}

		public object Clone()
		{
			return MemberwiseClone() as Detalles;
		}
	}
}
