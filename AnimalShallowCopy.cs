﻿using System;

namespace prototype_example
{
	class AnimalShallowCopy : ICloneable
	{
		public string Nombre { get; set; }
		public int Patas { get; set; }

		public AnimalShallowCopy(string nombre, int patas)
		{
			Nombre = nombre;
			Patas = patas;
		}

		public object Clone()
		{
			return MemberwiseClone() as AnimalShallowCopy;
		}

		public override string ToString()
		{
			return string.Format("Nombre: {0}, Nro de Patas: {1}", Nombre, Patas);
		}
	}
}
